/**
 * @format
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import moment from 'moment';
import 'moment/locale/pt-br';

import Chart from './Chart';
import MainScreen from './MainScreen';
import Settings from './Settings';
import TabBar from './components/TabBar';

moment.locale('pt-br');

const Tab = createBottomTabNavigator();

function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Home"
        tabBar={(props) => <TabBar {...props} />}
      >
        <Tab.Screen name="Chart" component={Chart} />
        <Tab.Screen name="Home" component={MainScreen} />
        <Tab.Screen name="Settings" component={Settings} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;
