/**
 * @format
 */

import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

const Line = ({ color }) => {
  return <View style={[styles.line, { backgroundColor: color }]} />;
};

const Card = (props) => {
  const { title, value, temperature, image } = props;

  const lineOn = value / 20;
  const lines = [...Array(5).keys()];
  const lineColors = lines
    .map((i, index) => {
      const color = index + 1 > lineOn ? '#9BA7B2' : '#1AFF92';
      return color;
    })
    .reverse();

  return (
    <View style={styles.cardWrap}>
      <View style={styles.card}>
        <Text style={styles.text}>{title}</Text>
        <View style={styles.leftContainer}>
          <View>
            {lineColors.map((color, index) => (
              <Line color={color} key={`key${index}`} />
            ))}
          </View>
          <View style={styles.contentContainer}>
            <Image style={styles.cardImage} source={image} />
            <View style={styles.statusContainer}>
              <Text style={styles.tempText}>{temperature.toFixed(1)}°C</Text>
              <Text style={styles.statusText}>
                {value.toFixed(1)}% Retantes
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  cardWrap: {
    padding: 6,
  },
  card: {
    backgroundColor: '#2B3137',
    borderRadius: 8,
    padding: 10,
    width: '100%',
  },
  leftContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  cardImage: {
    width: 80,
    height: 80,
  },
  text: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  contentContainer: {
    marginTop: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  statusContainer: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tempText: {
    color: '#FFF',
    fontSize: 24,
    fontWeight: 'bold',
  },
  statusText: {
    color: '#D2D8DD',
    fontSize: 18,
    marginTop: 8,
  },
  line: {
    height: 26,
    width: 8,
    borderRadius: 4,
    marginVertical: 9,
  },
});
