/**
 * @format
 */

import React from 'react';
import { StyleSheet, ScrollView, View, Text } from 'react-native';
import { LineChart } from 'react-native-chart-kit';

const Graph = ({ database, labels, title, yAxisSuffix }) => {
  return (
    <View style={styles.body}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <ScrollView horizontal>
        <LineChart
          data={{
            labels,
            datasets: [
              {
                data: database,
              },
            ],
          }}
          yAxisSuffix={yAxisSuffix}
          width={650}
          height={240}
          yAxisInterval={1}
          xLabelsOffset={-4}
          chartConfig={{
            backgroundColor: '#22272C',
            backgroundGradientFrom: '#22272C',
            backgroundGradientTo: '#22272C',
            decimalPlaces: 1,
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16,
            },
            propsForDots: {
              r: '4',
              strokeWidth: '2',
              stroke: '#ffa726',
            },
            propsForLabels: {
              fontSize: 11,
              padding: 10,
            },
            paddingRight: 30,
            propsForHorizontalLabels: {
              fontWeight: 'bold',
            },
            propsForVerticalLabels: {
              fontWeight: 'bold',
            },
          }}
          bezier
          style={styles.chart}
          // withVerticalLines={false}
          fromZero
          segments={6}
        />
      </ScrollView>
    </View>
  );
};

export default Graph;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#22272C',
  },
  scrollView: {
    flex: 1,
  },
  body: {
    paddingTop: 10,
  },
  chart: {
    transform: [{ translateX: -10 }, { translateY: -7 }],
  },
  titleContainer: {
    width: '100%',
    alignItems: 'center',
    marginBottom: 6,
  },
  title: {
    color: '#fff',
    fontSize: 18,
  },
});
