import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ProgressChart } from 'react-native-chart-kit';
import Ionicons from 'react-native-vector-icons/Ionicons';

const chartConfig = {
  backgroundGradientFrom: '#1E2923',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: '#08130D',
  backgroundGradientToOpacity: 0,
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity > 0.4 ? 1 : 0})`,
  strokeWidth: 2,
  barPercentage: 0.5,
};

const Progress = ({ title, value, max, min }) => {
  const data = {
    labels: [''],
    data: [value / 100],
  };

  return (
    <View style={styles.container}>
      <ProgressChart
        data={data}
        width={120}
        height={120}
        strokeWidth={8}
        radius={50}
        chartConfig={chartConfig}
        hideLegend
        style={{ transform: [{ rotate: '180deg' }] }}
      />
      <View style={styles.percentageContainer}>
        <Text style={styles.percentageText}>{value.toFixed(0)}%</Text>
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.leftRadius} />
        <View style={styles.titleContainer}>
          <Ionicons name="water-outline" size={25} color={'#FFF'} />
          <Text style={styles.title}>{title}</Text>
        </View>
        <View style={styles.subtitleContainer}>
          <Text style={styles.subtitle}>
            Max: {max.toFixed(0)}%{'   '}Min: {min.toFixed(0)}%
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Progress;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 10,
  },
  percentageContainer: {
    position: 'absolute',
    top: 40,
    left: 20,
    width: 80,
    alignItems: 'center',
  },
  percentageText: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    marginVertical: 8,
    backgroundColor: '#2B3137',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    overflow: 'hidden',
    paddingLeft: 28,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftRadius: {
    position: 'absolute',
    left: -140,
    top: -28,
    backgroundColor: '#22272C',
    height: 160,
    borderRadius: 80,
    width: 160,
  },
  title: {
    color: '#fff',
    fontSize: 22,
    marginLeft: 4,
  },
  subtitleContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  subtitle: {
    color: '#D2D8DD',
    fontSize: 16,
  },
});
