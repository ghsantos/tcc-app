import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Header = ({ title, goBack }) => (
  <View style={styles.header}>
    <View style={[styles.buttonContainer, styles.buttonLeftContainer]}>
      {!!goBack && (
        <TouchableOpacity style={styles.button}>
          <Icon name="arrow-back-ios" size={22} color="#F7F7F7" />
        </TouchableOpacity>
      )}
    </View>
    <View>{!!title && <Text style={styles.title}>{title}</Text>}</View>
    <View style={styles.buttonContainer} />
  </View>
);

export default Header;

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 44,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: 8,
  },
  button: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  buttonLeftContainer: {
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F7F7F7',
  },
});
