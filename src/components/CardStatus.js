/**
 * @format
 */

import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

const Card = (props) => {
  const { title, value, image } = props;

  const backgroundColor = value ? '#1AFF92' : '#FF5858';
  const statusText = value ? 'Ligada' : 'Desligada';

  return (
    <View style={styles.cardWrap}>
      <View style={styles.card}>
        <Text style={styles.text}>{title}</Text>
        <View style={styles.contentContainer}>
          <Image style={styles.cardImage} source={image} />
          <View style={styles.statusContainer}>
            <View style={[styles.status, { backgroundColor }]} />
            <Text style={styles.statusText}>{statusText}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  cardWrap: {
    padding: 6,
  },
  card: {
    backgroundColor: '#2B3137',
    borderRadius: 8,
    padding: 10,
    width: '100%',
  },
  cardImage: {
    width: 76,
    height: 76,
  },
  text: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  contentContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  statusContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  status: {
    width: 14,
    height: 14,
    borderRadius: 7,
  },
  statusText: {
    color: '#D2D8DD',
    fontSize: 17,
    marginTop: 6,
  },
});
