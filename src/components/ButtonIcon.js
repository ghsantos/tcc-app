import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';

const Button = ({ title, icon, select, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.button}>
    <EntypoIcon name={icon} size={18} color={select ? '#F4F5F6' : '#B1BAC3'} />
    <Text style={[styles.text, select && styles.select]}>{title}</Text>
  </TouchableOpacity>
);

export default Button;

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  text: {
    paddingTop: 4,
    color: '#B1BAC3',
    fontSize: 11,
  },
  select: {
    color: '#F4F5F6',
  },
});
