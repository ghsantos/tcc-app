import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

const Button = ({ title, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.button}>
    <Text style={styles.text}>{title}</Text>
  </TouchableOpacity>
);

export default Button;

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#2E343B',
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderRadius: 6,
  },
  text: {
    color: '#F7F7F7',
    fontSize: 18,
    fontWeight: 'bold',
  },
});
