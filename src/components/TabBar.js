import React from 'react';
import { View, StyleSheet } from 'react-native';

import ButtonIcon from './ButtonIcon';

const TabBar = ({ navigation, state }) => {
  const { index } = state;
  return (
    <View style={styles.tabbar}>
      <View style={[styles.buttonContainer, styles.buttonLeftContainer]}>
        <ButtonIcon
          title="Histórico"
          icon="line-graph"
          select={index === 0}
          onPress={() => navigation.navigate('Chart')}
        />
      </View>
      <ButtonIcon
        title="Home"
        icon="home"
        select={index === 1}
        onPress={() => navigation.navigate('Home')}
      />
      <View style={[styles.buttonContainer, styles.buttonRightContainer]}>
        <ButtonIcon
          title="Configurações"
          icon="sound-mix"
          select={index === 2}
          onPress={() => navigation.navigate('Settings')}
        />
      </View>
    </View>
  );
};

export default TabBar;

const styles = StyleSheet.create({
  tabbar: {
    width: '100%',
    height: 56,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 14,
    backgroundColor: '#22272C',
    elevation: 3,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  buttonLeftContainer: {
    justifyContent: 'flex-start',
  },
  buttonRightContainer: {
    justifyContent: 'flex-end',
  },
});
