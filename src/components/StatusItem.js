import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const StatusItem = ({ value, title, signal, icon }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.content}>
        <Ionicons name={icon} size={24} color={'#FFF'} />
        <Text style={styles.value}>
          {value?.toFixed(1)}
          {signal}
        </Text>
      </View>
    </View>
  );
};

export default StatusItem;

const styles = StyleSheet.create({
  container: {
    marginVertical: 4,
  },
  title: {
    color: '#D2D8DD',
    fontSize: 14,
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 2,
  },
  value: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 20,
    marginLeft: 4,
  },
});
