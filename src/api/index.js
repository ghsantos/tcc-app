import io from 'socket.io-client';

const api = 'http://192.168.0.110:3000';

export const getData = async () => {
  const result = await fetch(`${api}/data`);

  const parsedResult = await result.json();

  return parsedResult;
};

export const getHistory = async () => {
  const result = await fetch(`${api}/history`);

  const parsedResult = await result.json();

  return parsedResult;
};

export const socket = io(api);
