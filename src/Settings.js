/**
 * @format
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  Switch,
  Text,
  View,
} from 'react-native';

import { socket } from './api';
import Header from './components/Header';

const Item = ({ children }) => <View style={styles.item}>{children}</View>;

const Settings = ({ navigation }) => {
  const [bumpDisable, setBumpDisable] = useState(false);
  const [ledDisable, setLedDisable] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    socket.on('bumpDisable', (payload) => {
      setBumpDisable(payload);
      setLoading(false);
    });

    socket.on('ledDisable', (payload) => {
      setLedDisable(payload);
      setLoading(false);
    });
  }, []);

  const toggleBump = () => {
    socket.emit('bumpDisable', !bumpDisable);

    setBumpDisable(!bumpDisable);
  };

  const toggleLed = () => {
    socket.emit('ledDisable', !ledDisable);

    setLedDisable(!ledDisable);
  };

  const trackColor = { false: '#666374', true: '#235E51' };

  return (
    <>
      <StatusBar barStyle="ligth-content" backgroundColor="#22272C" />
      <SafeAreaView style={styles.safeArea}>
        <Header title="Configurações" />
        <ScrollView style={styles.scrollView}>
          <Item>
            <Text style={loading ? styles.textDisable : styles.text}>
              Desabilitar Bomba
            </Text>
            <Switch
              trackColor={trackColor}
              thumbColor={bumpDisable ? '#1AFF92' : '#B9B9B9'}
              onValueChange={toggleBump}
              value={bumpDisable}
              disabled={loading}
            />
          </Item>
          <Item>
            <Text style={loading ? styles.textDisable : styles.text}>
              Desabilitar Iluminação
            </Text>
            <Switch
              trackColor={trackColor}
              thumbColor={ledDisable ? '#1AFF92' : '#B9B9B9'}
              onValueChange={toggleLed}
              value={ledDisable}
              disabled={loading}
            />
          </Item>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#22272C',
  },
  scrollView: {
    paddingTop: 10,
    flex: 1,
  },
  item: {
    padding: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 16,
    color: '#FFF',
  },
  textDisable: {
    fontSize: 16,
    color: '#AAA',
  },
});

export default Settings;
