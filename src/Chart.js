/**
 * @format
 */

import React, { useEffect, useCallback, useState } from 'react';
import {
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
} from 'react-native';
import moment from 'moment';

import { getHistory } from './api';
import Graph from './components/Graph';
import Header from './components/Header';

const getLabels = (data) => {
  const dataLabels = data.map((i, index) => {
    if (index % 3) {
      return '';
    }

    const date = moment(i);

    return date.format('LT');
  });

  return dataLabels;
};

const MainScreen = ({ navigation }) => {
  const [history, setHistory] = useState(null);
  const [labels, setLabels] = useState(null);
  const [refreshing, setRefreshing] = React.useState(false);

  const fetchData = useCallback(async () => {
    setRefreshing(true);
    const result = await getHistory();
    const utc = getLabels(result.utc);

    setHistory(result);
    setLabels(utc);
    setRefreshing(false);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <>
      <StatusBar barStyle="ligth-content" backgroundColor="#22272C" />
      <SafeAreaView style={styles.safeArea}>
        <Header title="Histórico" />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={fetchData} />
          }
        >
          {!!history && !!labels && (
            <>
              <Graph
                database={history.bottomTemperatureHistory}
                labels={labels}
                title="Temperatura Interna"
                yAxisSuffix="°C"
              />
              <Graph
                database={history.externalTemperatureHistory}
                labels={labels}
                title="Temperatura Externa"
                yAxisSuffix="°C"
              />
              <Graph
                database={history.bottomHumidityHistory}
                labels={labels}
                title="Umidade Interna"
                yAxisSuffix="%"
              />
              <Graph
                database={history.externalHumidityHistory}
                labels={labels}
                title="Umidade Externa"
                yAxisSuffix="%"
              />
              <Graph
                database={history.waterTemperatureHistory}
                labels={labels}
                title="Temperatura da Água"
                yAxisSuffix="°C"
              />
            </>
          )}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#22272C',
  },
  scrollView: {
    flex: 1,
  },
  body: {
    paddingTop: 10,
  },
  chart: {
    transform: [{ translateX: -10 }, { translateY: -7 }],
  },
  titleContainer: {
    width: '100%',
    alignItems: 'center',
    marginBottom: 6,
  },
  title: {
    color: '#fff',
    fontSize: 18,
  },
});

export default MainScreen;
