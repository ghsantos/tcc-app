/**
 * @format
 */

import React, { useState, useEffect, useCallback, useRef } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';

import { getData } from './api';
import Card from './components/CardStatus';
import CardTank from './components/CardTank';
import Header from './components/Header';
import ProgressChart from './components/ProgressChart';
import StatusItem from './components/StatusItem';

const alface = require('./assets/alface.png');
const ledImage = require('./assets/green-energy.png');
const pumpImage = require('./assets/pump.png');
const waterTank = require('./assets/water-tank.png');

const MainScreen = ({ navigation }) => {
  const [data, setData] = useState(null);
  const interval = useRef(null);

  const fetchData = useCallback(async () => {
    const result = await getData();
    setData(result);
  }, []);

  useEffect(() => {
    if (interval.current) {
      clearInterval(interval.current);
    }
    fetchData();

    interval.current = setInterval(() => {
      fetchData();
    }, 1000 * 20);

    return () => clearInterval(interval.current);
  }, [fetchData]);

  const internalHumidity = data
    ? (data.topHumidity + data.bottomHumidity) / 2
    : 0;
  const internalTemperature = data
    ? (data.topTemperature + data.bottomTemperature) / 2
    : 0;

  return (
    <>
      <StatusBar barStyle="ligth-content" backgroundColor="#22272C" />
      <SafeAreaView style={styles.safeArea}>
        <Header title="Hydrobee" />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}
        >
          {!!data && (
            <>
              <View style={styles.imgContainer}>
                <View style={styles.imageContainer}>
                  <Image style={styles.image} source={alface} />
                </View>
                <View style={styles.statusItem}>
                  <StatusItem
                    value={internalTemperature}
                    title="Temperatura interna"
                    signal="°C"
                    icon="thermometer-sharp"
                  />
                  <StatusItem
                    value={data.externalTemperature}
                    title="Temperatura externa"
                    signal="°C"
                    icon="thermometer-sharp"
                  />
                  <StatusItem
                    value={data.luminance}
                    title="Luminosidade"
                    signal=" Lúmens"
                    icon="md-sunny-outline"
                  />
                </View>
              </View>
              <View style={styles.contentContainer}>
                <ProgressChart
                  title="Umidade interna"
                  value={internalHumidity}
                  max={data.maxInternalHumidity}
                  min={data.minInternalHumidity}
                />
                <ProgressChart
                  title="Umidade externa"
                  value={data.externalHumidity}
                  max={data.maxExternalHumidity}
                  min={data.minExternalHumidity}
                />
              </View>
              <View style={styles.body}>
                <View style={styles.cardContainer}>
                  <Card
                    title="Bomba de água"
                    value={data.bump}
                    image={pumpImage}
                  />
                  <Card title="Iluminação" value={data.led} image={ledImage} />
                </View>
                <View style={styles.cardContainer}>
                  <CardTank
                    title="Reservatório"
                    value={data.tank}
                    temperature={data.waterTemperature}
                    image={waterTank}
                  />
                </View>
              </View>
            </>
          )}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#22272C',
  },
  imgContainer: {
    backgroundColor: '#2B3137',
    borderRadius: 20,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  imageContainer: {
    justifyContent: 'center',
  },
  image: {
    width: 190,
    height: 190,
  },
  statusItem: {
    padding: 20,
    marginLeft: 8,
    justifyContent: 'center',
  },
  statusHeight: {
    height: 190,
  },
  scrollView: {
    flex: 1,
  },
  body: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#22272C',
  },
  cardContainer: {
    flex: 1,
  },
  contentContainer: {
    backgroundColor: '#22272C',
  },
});

export default MainScreen;
